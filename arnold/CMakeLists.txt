set(SHADER alVolume)

include_directories("..")

set(SRC ${SHADER}.cpp ../common/Color.cpp)
set(MTD ${SHADER}.mtd)
set(UI ${SHADER}Template.py)

add_library(${SHADER} SHARED ${SRC})

target_link_libraries(${SHADER} ai VolumeCache field3d hdf5)
set_target_properties(${SHADER} PROPERTIES PREFIX "")

install(TARGETS ${SHADER} DESTINATION ${MTOA_SHADERS})
install(FILES ${MTD} DESTINATION ${MTOA_SHADERS})
install(FILES ${UI} DESTINATION ${MTOA_UI})

